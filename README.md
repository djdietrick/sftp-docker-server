## To generate keys

### Generate host (container) keys
This makes the ssh keys used by the container constant between each run. Otherwise it will generate new keys and the host will not be the same as the known host when connecting.

`ssh-keygen -t rsa -b 4096 -f ./keys/ssh_host_rsa_key < /dev/null`

### Generate client keys
These are the keys used by the client to connect to the server.

`ssh-keygen -t rsa -b 4096 -f ./keys/id_rsa` <br>
`ssh-keygen -f ./keys/id_rsa -e -m pem > ./keys/id_rsa_privkey.pem`

## Run the process

`docker-compose up`

## To connect

`sftp -o IdentityFile=./keys/id_rsa_privkey.pem -P 2222 foo@localhost`